#!/usr/bin/env python3
'''asynchronuously (re-)render all html/pdf-files in this project

requires hovercraft, pyscss, rst2html, rst2pdf, sed

If you are not developing the Python Beginner's Workshop itself, you should not
need this. There are better ways to achieve the same results (build tools!),
but I had fun writing this. Never tested on Windows.

'''
import asyncio
from asyncio.subprocess import PIPE
from pathlib import Path
from shlex import quote
from sys import argv, platform
from textwrap import wrap


@asyncio.coroutine
def prepare(*args):
    args = [str(arg) for arg in args]
    shell_cmd = ' \\\n      '.join(quote(arg) for arg in args)
    create = asyncio.create_subprocess_exec(*args, stdout=PIPE, stderr=PIPE)

    try:
        proc = yield from create
    except Exception as ex:
        print('FAIL:', shell_cmd)
        print('INFO:', ex, '\n')
        return

    stdout, stderr = yield from proc.communicate()
    if not proc.returncode:
        print('OK  :', shell_cmd, '\n')
    else:
        print('FAIL:', shell_cmd)
        print('INFO:', '\n      '.join(stderr.decode().splitlines()), '\n')


@asyncio.coroutine
def sequential(*tasks):
    for task in tasks:
        yield from task


if __name__ == '__main__':
    print(__doc__)
    here = Path(__file__).resolve().parent
    tasks = [
        # presentation
        sequential(
            # re-create css from scss
            prepare('pyscss',
                    '--no-compress',
                    here / 'presentation/src/css/style.scss',
                    '--output',
                    here / 'presentation/src/css/style.css'),
            # build presentation
            prepare('hovercraft',
                    here / 'presentation/src/python-intro-presentation.rst',
                    here / 'presentation/html'),
            # What is this, a presentation for ANTS!?  Patch impress.js.
            # Results in blurry slides. By GOD, all of this is UGLY!
            prepare('sed',
                    '--in-place',
                    '--regexp-extended',
                    r's/^(\s*)windowScale = .*;$/\1windowScale = 1.5;/',
                    here / 'presentation/html/js/impress.js'),
        ),
        # tutorial
        prepare('rst2html',
                here / 'tutorial/python-intro-tutorial.rst',
                here / 'tutorial/python-intro-tutorial.html'),
        prepare('rst2pdf',
                here / 'tutorial/python-intro-tutorial.rst',
                here / 'tutorial/python-intro-tutorial.pdf'),
    ]
    loop = asyncio.SelectorEventLoop() if platform != 'win32' else asyncio.ProactorEventLoop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
