Python Beginner's Workshop
==========================

Repository Contents
-------------------

.. code:: shell

    .
    ├── data          # sample data file(s)
    │
    ├── presentation  # introductory presentation
    │   ├── html      #   ... slides rendered as HTML
    │   └── src       #   ... source files (to build the slides yourself)
    │
    └── tutorial      # python tutorial
