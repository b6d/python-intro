====================================
Python Beginner's Workshop: Tutorial
====================================

`Alexandre Savio <mailto:alexsavioATgmail.com>`_

`Bernhard Weitzhofer <mailto:bernhardATweitzhofer.org>`_

`PyCon.de <http://www.pymunich.com>`_ 2016-10-29


Running Python
==============

Assuming you have Python 3 installed, under BSD, Linux, or OSX just enter

.. code:: shell-session

    $ python3

at your terminal. Under Windows, enter

.. code:: shell-session

   $ python

or click the Python-Icon in your apps.

.. note::

    You don't have to enter the ``$`` character. It just represents your
    terminal's input prompt.

You'll be greeted with an interactive Python console looking something like
this:

.. code:: pycon

     Python 3.4.2 (default, Oct  8 2014, 10:45:20)
     [GCC 4.9.1] on linux
     Type "help", "copyright", "credits" or "license"
     for more information.
     >>>

.. note::

    If this doesn't work, possible causes and solutions are:

    - Python 3 isn't installed: use your operating system's package manager or
      see https://python.org/downloads/

    - Windows: Your system doesn't know where to find ``python.exe``. Do (one of
      the following:

        - provide the full path to ``python.exe`` (``C:\...\python.exe``)

        - add the *directory* where ``python.exe`` is located to your ``PATH``
          environment variable

      See https://docs.python.org/3/using/windows.html for more on Python 3 on
      Windows.

    - If everything fails, point your browser to https://python.org/shell as a
      last resort. You may not be able to follow all examples (especially the
      ones about reading files) but it's better than nothing.

At Python's input prompt ``>>>``, you can enter Python commands:

.. code:: pycon

    >>> print("Hello PyCon")
    Hello PyCon
    >>>

If you enter a command, it is immediately evaluated, the result (if any) is
printed and you are prompted to enter the next command.

.. code:: pycon

    >>> 1 + 1
    2
    >>>

This is called a Read-Eval-Print-Loop (REPL).

.. note::

    To quit, enter ``exit()`` or ``quit()`` or press the ``Ctrl+D``.


Python Programs
===============

Every command you enter in Pythons REPL, you may write into a file as well.
Save the file as ``my_program.py`` (or any other name, as long as it has the
extension ``.py``.)

``my_program.py`` contents:

.. code:: python

    print("Hello PyCon")

Congratulations! You've just written your first Python program.

Under BSD, Linux or OSX, run the programm by telling python the Python
interpreter to execute it:

.. code:: shell-session

    $ python3 my_program.py
    Hello PyCon

Under Windows, either start the Python IDE "IDLE":

.. code:: shell-session

    $ python.exe my_program.py
    Hello PyCon

.. note::

    make sure you provide the correct path to ``my_program.py`` if you are in
    a different directory.

.. note::

    The only notable difference between running the Python REPL interactively
    and running Python programs is that the REPL reports the result of every
    expression back to you. In Python programs, you have to use the ``print()``
    function explicitly.


Comments
========

Everything after a ``#``-character gets ignored by Python. You can use this to
add comments to your code (more useful in programs than in interactive code)

.. note::

    If the prompt at any time changes from ``>>>`` to ``...`` it means the
    Python REPL allows another line of input for the current block of code.
    Enter an empty line (Press the "Enter"-key), to unequivocally complete the
    current block of code:

    .. code:: pycon

        >>> # This is a comment. Python will ignore it.
        ... # Another comment. Provide empty line to finish current block
        ...
        >>> 1 + 1  # A comment. The expression before it will be evaluated.
        2


Integers
========

No surprises there:

.. code:: pycon

    >>> 3 + 2
    5
    >>> (3 + 2) * 2
    10


Strings
=======

May be single or double quoted. Use what's convenient:

.. code:: pycon

    >>> 'Hello PyCon'
    'Hello PyCon'
    >>> "Hello PyCon"
    'Hello Pycon'
    >>> "Joe's diner"
    "Joe's diner"
    >>> 'The cow says "moo"'
    'The cow says "moo"'
    >>> 'Hello ' + "PyCon"
    'Hello Pycon'


Naming Things (Variables)
=========================

Python's variables are names for things.  A ``=`` gives a name to the result of
the expression on the right.

.. code:: pycon

    >>> weight_plane = 2000
    >>> weight_fuel = 100

Once named, you can use those names instead of the values they represent

.. code:: pycon

    >>> weight_plane + weight_fuel
    2100
    >>> total_weight = weight_plane + weight_fuel
    >>> total_weight
    2100

You may redefine names (variables), but be careful:

.. code:: pycon

    >>> weight_plane = 1000
    >>> weight_plane
    1000
    >>> total_weight   # careful! total_weight hasn't changed
    2100


Types
=====

Every thing in python is of a certain type. Use ``type`` to query the type of
any object you encounter (For those of you familiar with object oriented
programming: in Python, everything is an Object)

.. code:: pycon

    >>> type(1)
    <class 'int'>
    >>> type(1.5)
    <class 'float'>
    >>> type('Hello')
    <classs 'str'>

Where it makes sense, values of different types play well together:

.. code:: pycon

    >>> 1 + 1.5
    2.5
    >>> type(1)
    <classs 'int'>
    >>> type(1.5)
    <classs 'float
    >>> type(1 + 1.5)
    <classs 'float'>
    >>> 'toi ' * 3
    'toi toi toi '
    >>> type('toi ' * 3)
    <classs 'str'>

Where it doesn't make sense, Python complains:

.. code:: pycon

    >>> 3 + 'foo'
    #Traceback (most recent call last):
    #  File "<stdin>", line 1, in <module>
    #TypeError: unsupported operand type(s) for +: 'int' and 'str'


Functions (and other callables)
===============================

In Python, functions are predefined program pieces.

Python functions may *do* stuff (``print`` outputs text to the screen), they
may *return* stuff (``len`` *returns* the length of things like strings for
example), or they may do both.

Use parantheses ``()`` after the name of a function to *invoke* (call) that
function and make it work its magic:

.. code:: pycon

    >>> len('python')
    6
    >>> abs(-3)
    3
    >>> min(1, 2)
    1
    >>> min(1, 2, 0)
    0

.. note::

    See https://docs.python.org/3/library/functions.html#built-in-funcs for a
    list of built in functions.


.. admonition:: Extra

    Some function-like callables (called *methods* in Object oriented
    programming) are strongly associated only with objects of a certain type.
    A String's ``upper`` method, for example, may be used to uppercase a
    string:

    .. code:: pycon

        >>> event_today = 'PyCon'
        >>> event_today.upper()
        'PYCON'
        >>> 'Python'.upper()
        'PYTHON'


Batteries included
==================

Python provides a *lot* of functionality right out of the box (see
https://docs.python.org/3/library/index.html) for an overview. We won't use
those in this tutorial, but for completeness' sake, here's an example.

.. code:: pycon

    >>> import math
    >>> math.pi
    3.141592653589793
    >>> math.cos(0)
    1.0


Lists
=====

Lists are collections of values. You can create a list using square brackets ``[]``:

.. code:: pycon

    >>> fruits = ['apple', 'orange', 'pear', 'lemon']
    >>> fruits
    ['apple', 'orange', 'pear', 'lemon']
    >>> len(fruits)
    4
    >>> type(fruits)
    <classs 'list'>

You may retrieve lists elements via their indices using square brackets ``[]``
as well:

.. code:: pycon

    >>> fruits[0]
    'apple'
    >>> fruits[1]
    'orange'
    >>> fruits[3]
    'lemon'
    >>> fruits[-1]
    'lemon'
    >>> fruits[-2]
    'pear'
    >>> fruits[1:3]
    ['orange', 'pear']

List elements may be of any type. You can mix and match as you like (lists of
lists are possible as well):

.. code:: pycon

    >>> my_crazy_list = ['one', 'two', 3, ['foo', 'bar', 5.4]]

Lists are mutable:

.. code:: pycon

    >>> fruits = ['apple', 'banana', 'orange']
    >>> fruits[2] = pear'
    >>> fruits
    ['apple', 'banana', 'pear']
    >>> fruits.append('lemon')
    >>> fruits
    ['apple', 'banana', 'pear', 'banana']


Strings are collections too
===========================

If you think about it, a string is no more than a collection of characters (and
in Python, a character is no more than a String having a length of 1)

.. code:: pycon

    >>> name = 'python'
    len(name)
    6
    >>> name[0]
    'p'
    >>> name[-n]
    'n'
    >>> name[1:4]
    'yth'


Other Collections
=================

Lists and Strings are not the only collections in Python. There are more
(tuples, dictionarys, sets, ...), but we won't bother with them for now.


Looping over Collections
========================

To do something with each element in a collection (or anything else that can be
iterated over), use a for loop:

.. code:: python

    for fruit in ['apple', 'banana', 'orange']:
         print(fruit)

The first line of a for loop starts with ``for``, followed a variable name,
followed by a collection, followed by a colon ``:``

Indentation is used to indicate the loop body, that gets executed for every
element in the list.

.. admonition:: Wait, what? Whitespace has MEANING?!

    Yes, in Python (in some cases) it does. Form follows function. Try to stick
    to an indentation of 4 spaces. Avoid mixing tabs with spaces. You'll get
    used to it.

In the REPL, this looks like this:

.. code:: pycon

    >>> for fruit in ['apple', 'banana', 'orange']:
    ...     print(fruit)
    ...
    apple
    banana
    orange


Conditionals
============

To execude code only if certain condtions are met, use an ``if`` statement. It
has the form

.. code:: python

    if total_weight < 3000:
        print('Ready for takeoff')


The first line of a conditional starts with an ``if``, followed by some test,
followed by a colon ``:``

Again, indentation is used to indicate which lines will be executed if the test
succeeds.

In the REPL, this looks like this:

.. code:: pycon

    >>> total_weight = 2000
    >>> if total_weight < 3000:
    ...     print('Ready for takeoff')
    ...
    Ready for takeoff

Tests
-----

Some tests that may be performed in the first line of an ``if``-statement:

.. code:: pycon

    >>> 1 <= 2
    True
    >>> 'hello' == 'hello'
    True
    >>> 'hello' == "hello"
    True
    >>> 'hello' == 'Hello'
    False
    >>> 'banana' in ['apple', 'orange', 'banana']:
    True
    >>> 'apple' not in ['apple', 'orange', 'banana']:
    False
    >>> 'r' in 'orange'
    True
    >>> 'ange' in 'orange'
    True
    >>> 'app' in 'orange'
    False
    >>> total_weight = 2000
    >>> 1000 < total_weight and total_weight < 3000
    True
    >>> 1000 < total_weight < 3000
    True
    >>> lang = 'Python'   # assignment, doesn't return anything
    >>> lang == 'Python'  # test
    True
    >>> lang == 'ython'
    False


Nesting Blocks
==============

Blocks may be nested. Indent accordingly.

.. code:: python

    for weight in [2200, 1800, 3800, 1500, 9000]:
        print('Weight is:', weight)
        if weight < 3000:
            print('Ready for takeoff')
        else:
            print('Too heavy')


Defining Functions
==================

You may define your own functions like this:

.. code:: python

     def say_hello():
         print('hello')
         print('goodbye')

Again, indentation indicates the code block making up the body of the function.

Function arguments may be specified, comma-separated, between the parentheses.
The ``return`` statement lets the function return a value.

.. code:: python

    def my_add(a, b):
        return a + b

Once a function is defined, it may be called like any of the functions we've
seen before:

.. code:: pycon

    >>> my_add(1, 2)
    3
    >>> my_add('Hello ', 'Pycon')
    'Hello Pycon!'


Reading Files
=============

Open, read, and close a textfile:

.. code:: python

    my_file = open('somefile.txt')
    content = my_file.read()
    # maybe do other stuff
    close(my_file)

    print(content)

Let Python take care of closing the file (the preferred method):

.. code:: python

    with open('somefile.txt') as my_file:
        content = my_file.read()
        # maybe do other stuff

    print(content)

Process a file line by line:

.. code:: python

    with open('somefile.txt') as my_file:
        for line in my_file:
            print(line)
            # maybe do other stuff
