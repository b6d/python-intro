=======
LICENSE
=======

Everything in this repository not covered by other licenses (see 3rd Party
Content) is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License (http://creativecommons.org/licenses/by-sa/4.0/).

By contributing to this project, you agree to license your contributions under
CC-SA 4.0 as well.


3rd Party Content
=================

Image "Python 3 Windows Installation"
-------------------------------------
Via Django Girls Tutorial, CC-SA 4.0


Font Droid Sans Mono
--------------------

Via Google Fonts, Apache License v2.0


Font Libre Baskerville
----------------------

Via Google Fonts, Apache License Open Font License


Grimm's Tales
-------------

Via Project Gutenberg, Project Gutenberg License
