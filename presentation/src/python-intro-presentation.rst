:title: Python Intro
:data-transition-duration: 0
:css: css/style.css
:skip-help: true

.. This File is part of the Python Beginner's Workshop

   You can get the latest version of the workshop from

   https://gitlab.com/b6d/python-intro


.. This is a source file for the Hovercraft presentation tool.
   (https://pypi.python.org/pypi/hovercraft/)

   To render this presentation to HTML, use the command

   $ hovercraft python-intro-presentation.rst outdir

   and then view the outdir/index.html file to see how it turned out.


----


Welcome to the Python Beginner's Workshop
=========================================

`Alexandre Savio <mailto:alexsavioATgmail.com>`_

`Bernhard Weitzhofer <mailto:bernhardATweitzhofer.org>`_

`PyCon.de <http://www.pymunich.com>`_ 2016-10-29


----


Python is a Programming Language
================================


----


You already use Python
======================

Amazon, Apple, Dropbox, EVE Online, Facebook, Google, Instagram, Microsoft,
NASA, NSA(☹), Netflix, reddit, PayPal, Snapchat, Star Wars, Uber, World of
Tanks, YouTube and many, many more, ...

.. sources:
   https://www.python.org/about/success/
   http://stackshare.io/python/in-stacks
   https://en.wikipedia.org/wiki/List_of_Python_software


----


Python has a history
====================

- started 25 years ago
- major overhaul 8 years ago (Python 3)


----


Python has a future
===================

- #1 intro language @ Top 100 US Universities

- Industry Support

- Great Community

- You

.. sources:
   http://cacm.acm.org/blogs/blog-cacm/176450


----


Python is Opinionated
=====================

"The Zen of Python" [`PEP20 <https://www.python.org/dev/peps/pep-0020>`_]


----


Python tries to be
==================

- Readable

- Beautiful

.. source: PEP20


----


For this workshop
=================

I say
    Python

I mean
    Python 3


----


Do I have Python already?
=========================

Linux, OSX:

.. code:: shell-session

    $ python3 --version
    Python 3.5.1

----


Do I have Python already?
=========================

Windows:

.. image:: ./img/windows-python-app-icons.png
    :alt: [source: screenshot]
    :width: 50%



----


Installation (Linux)
====================

.. code:: shell-session

    $ # Debian-based (Ubuntu, Mint, ...)
    $ sudo apt-get install python3

    $ # RedHat-based (Fedora et.al., ...)
    $ sudo yum install python3
    $ sudo dnf install python3

    $ # SUSE-based
    $ sudo zypper install python3

    $ # Arch-based
    $ # you already have python3


----


Install (Win, OSX)
==================

https://python.org/download

.. image:: ./img/windows-python-installation.png
    :alt: [Source Django Girls Tutorial, CC SA 4.0]
    :width: 90%


----


Running Python interactively
============================

Linux, OSX

.. code:: shell-session

    $ python3

Windows:

.. code:: shell-session

    $ python

(or click the icon)



----


Workshop Material
=================

.. code:: shell-session

    $ git clone https://gitlab.com/b6d/python-intro

or:

- Point Browser to URL above
- Click Dowload Icon
